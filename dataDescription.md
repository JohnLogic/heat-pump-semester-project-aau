
## HVAC dataset
* HVAC_DehumidifierAirflow - Average airflow through dedicated dehumidifier over the course of the minute	ft3/min
* HVAC_DehumidifierExitAirTemp - Average temperature of air in outlet duct from dedicated dehumidifier over the course of the minute	°F
* HVAC_DehumidifierInletAirTemp - Average temperature of air in inlet duct to dedicated dehumidifier over the course of the minute	°F
* HVAC_DehumidifierPower - Average power consumption of dedicated dehumidifier over the course of the minute	W
* HVAC_HeatPumpIndoorUnitPower - Average power consumption of heat pump indoor unit (blower, controls, resistance heat) over the course of the minute	W
* HVAC_HeatPumpOutdoorUnitPower - Average power consumption of heat pump outdoor unit (compressor, fan, controls, defrost) over the course of the minute W
* HVAC_HVACDewpointReturnAir - Instantaneous dewpoint temperature of air in return duct prior to indoor unit	°C
* HVAC_HVACDewpointSupplyAir - Instantaneous dewpoint temperature of air in supply duct after indoor unit	°C
* HVAC_HVACTempReturnAir - Instantaneous dry bulb temperature of air in return duct prior to indoor unit	°C
* HVAC_HVACTempSupplyAir - Instantaneous dry bulb temperature of air in supply duct after indoor unit	°C


## OutEnv dataset
* _OutEnv_OutdoorAmbTemp_	-
Air	Instantaneous dry bulb temperature measured above the roof's peak	°C
* _OutEnv_RooftopWindDirection_ - 
Air	Instantaneous wind direction measured above the roof's peak; 0° means wind is blowing from true north; all other readings are taken relative to clockwise from true north.  For example, 90° means wind is blowing from true east to true west.   	degrees
* _OutEnv_RooftopWindSpeed_ - 
Air	Instantaneous wind speed measured above the roof's peak	m/s
