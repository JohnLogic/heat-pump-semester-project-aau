## 10-08
* Find out whether outdoor temp is legit - assume yes - YEP
* Find out the labels for power and indoor temp YEP
* Load indoor temperature dataset
* Filter out all indoor temps and collect an average
* Plot
    - Indoor (averaged) vs outdoor
    - Heatpump power (total) 
    - All indoor
* Write some problem statements
    - Look at examples

* NEAT: write-up a script that would "prepare data folder" (we don't actually want to commit anything)