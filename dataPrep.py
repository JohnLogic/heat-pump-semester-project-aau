import pandas as pd
import numpy as np

# NOTES:
# 1. Load with index_col false, allows sorting (just in case)
    # NOTE: we need to sort this out in the future, lots of hackery went on with timestamp column and index

HVAC_DATA_HOUR_FILE = "site-data/HVAC-hour.csv"
OUTENV_DATA_HOUR_FILE = "site-data/OutEnv-hour.csv"
INDENV_DATA_HOUR_FILE = "site-data/IndEnv-hour.csv"

HVAC_DATA_MINUTE_FILE = "site-data/HVAC-minute.csv"
OUTENV_DATA_MINUTE_FILE = "site-data/OutEnv-minute.csv"
INDENV_DATA_MINUTE_FILE = "site-data/IndEnv-minute.csv"

POWER_COL_NAME = 'SumPower'
INDOOR_TEMP_COL_NAME = 'AvgIndoorTemp'
OUTDOOR_TEMP_COL_NAME = "OutdoorTemp"
EXTRA_TIMESTAMP_COL_NAME = "TS"

PREPARED_OUTPUT_DATASET_FILENAME = 'output/prepared_dataset.csv'

# Values: False OR ['Timestamp']
INDEX_COL_VAL = ['Timestamp']

HVAC_DATA_FILE = HVAC_DATA_HOUR_FILE
OUTENV_DATA_FILE = OUTENV_DATA_HOUR_FILE
INDENV_DATA_FILE = INDENV_DATA_HOUR_FILE

# Timestamp,
# HVAC_HVACDewpointReturnAir,
# HVAC_HVACDewpointSupplyAir,
# HVAC_HVACTempReturnAir,
# HVAC_HVACTempSupplyAir,
# HVAC_HeatPumpIndoorUnitPower,
# HVAC_HeatPumpOutdoorUnitPower,
# HVAC_DehumidifierAirflow,
# HVAC_DehumidifierExitAirTemp,
# HVAC_DehumidifierInletAirTemp,
# HVAC_DehumidifierPower
def open_hvac_data(hvac_data_file):
    hvac_data = pd.read_csv(hvac_data_file, delimiter=',', encoding='utf-8', parse_dates=['Timestamp'],
                      dtype={'Timestamp': str, 'HVAC_HVACDewpointReturnAir': float, 'HVAC_HVACDewpointSupplyAir': float,
                             'HVAC_HVACTempReturnAir': float, 'HVAC_HVACTempSupplyAir': float,
                             'HVAC_HeatPumpIndoorUnitPower': float, 'HVAC_HeatPumpOutdoorUnitPower': float,
                             'HVAC_DehumidifierAirflow': float, 'HVAC_DehumidifierExitAirTemp': float,
                             'HVAC_DehumidifierInletAirTemp': float, 'HVAC_DehumidifierPower': float}
                            # , index_col=['Timestamp']
                            , index_col=INDEX_COL_VAL )
    modified = hvac_data.filter(regex=(".*UnitPower.*")) #We filter out only unit power cols
    modified[POWER_COL_NAME] = modified.sum(numeric_only=True, axis=1)
    modified[EXTRA_TIMESTAMP_COL_NAME] = modified.index
    return modified

# Timestamp,
# OutEnv_OutdoorAmbTemp,
# OutEnv_RooftopWindDirection,
# OutEnv_RooftopWindSpeed
def open_outenv_data(outenv_data_file):
    csv = pd.read_csv(outenv_data_file, delimiter=',', encoding='utf-8', parse_dates=['Timestamp'],
                      dtype={'Timestamp': str, 'OutEnv_OutdoorAmbTemp': float, 'OutEnv_RooftopWindDirection': float,
                             'OutEnv_RooftopWindSpeed': float}
                              # , index_col=['Timestamp']
                               , index_col=INDEX_COL_VAL
)
    csv[EXTRA_TIMESTAMP_COL_NAME] = csv.index
    return csv

# Timestamp,
# IndEnv_BasementRH,
# IndEnv_Bedroom2RH,
# IndEnv_Bedroom3RH,
# IndEnv_KitchenRH,
# IndEnv_LivingRmRH,
# IndEnv_MBARH,
# IndEnv_MastBedRmRH,
# IndEnv_RadiantTempBR2,
# IndEnv_RadiantTempBR3,
# IndEnv_RadiantTempKitchen,
# IndEnv_RadiantTempLR,
# IndEnv_RadiantTempMBR,
# IndEnv_RoomTempAtticNE,
# IndEnv_RoomTempAtticNW,
# IndEnv_RoomTempAtticSE,
# IndEnv_RoomTempAtticSW,
# IndEnv_RoomTempBA1Temp,
# IndEnv_RoomTempBA2Temp,
# IndEnv_RoomTempBR2Temp,
# IndEnv_RoomTempBR3Temp,
# IndEnv_RoomTempBR4Temp,
# IndEnv_RoomTempBasementNE,
# IndEnv_RoomTempBasementNW,
# IndEnv_RoomTempBasementSE,
# IndEnv_RoomTempBasementSW,
# IndEnv_RoomTempDRTemp,
# IndEnv_RoomTempHallLowerMid,
# IndEnv_RoomTempHallLowest,
# IndEnv_RoomTempHallMiddle,
# IndEnv_RoomTempHallUpper,
# IndEnv_RoomTempHallUpperMid,
# IndEnv_RoomTempKitchenTemp,
# IndEnv_RoomTempLRTemp,
# IndEnv_RoomTempMBATemp,
# IndEnv_RoomTempMBRTemp,
# IndEnv_RoomTempWDTemp


def open_indenv_data(indenv_data_file):
    ind_env_data = pd.read_csv(indenv_data_file, delimiter=',', encoding='utf-8', parse_dates=['Timestamp'],
                      dtype={'Timestamp': str, 'IndEnv_BasementRH': float, 'IndEnv_Bedroom2RH': float,
                             'IndEnv_Bedroom3RH': float, 'IndEnv_KitchenRH': float, 'IndEnv_LivingRmRH': float,
                             'IndEnv_MBARH': float, 'IndEnv_MastBedRmRH': float, 'IndEnv_RadiantTempBR2': float,
                             'IndEnv_RadiantTempBR3': float, 'IndEnv_RadiantTempKitchen': float,
                             'IndEnv_RadiantTempLR': float, 'IndEnv_RadiantTempMBR': float,
                             'IndEnv_RoomTempAtticNE': float, 'IndEnv_RoomTempAtticNW': float,
                             'IndEnv_RoomTempAtticSE': float, 'IndEnv_RoomTempAtticSW': float,
                             'IndEnv_RoomTempBA1Temp': float, 'IndEnv_RoomTempBA2Temp': float,
                             'IndEnv_RoomTempBR2Temp': float, 'IndEnv_RoomTempBR3Temp': float,
                             'IndEnv_RoomTempBR4Temp': float, 'IndEnv_RoomTempBasementNE': float,
                             'IndEnv_RoomTempBasementNW': float, 'IndEnv_RoomTempBasementSE': float,
                             'IndEnv_RoomTempBasementSW': float, 'IndEnv_RoomTempDRTemp': float,
                             'IndEnv_RoomTempHallLowerMid': float, 'IndEnv_RoomTempHallLowest': float,
                             'IndEnv_RoomTempHallMiddle': float, 'IndEnv_RoomTempHallUpper': float,
                             'IndEnv_RoomTempHallUpperMid': float, 'IndEnv_RoomTempKitchenTemp': float,
                             'IndEnv_RoomTempLRTemp': float, 'IndEnv_RoomTempMBATemp': float,
                             'IndEnv_RoomTempMBRTemp': float, 'IndEnv_RoomTempWDTemp': float}
                               # , index_col=['Timestamp']
                               , index_col=INDEX_COL_VAL
                               )
    modified = ind_env_data.filter(regex=(".*RoomTemp.*")) # We filter out only indoor temperature columns
    for col in modified.columns:
        modified = modified[modified[col].between(-30.0, 30.0)]
    modified[INDOOR_TEMP_COL_NAME] = modified.mean(numeric_only=True, axis=1)
    modified[EXTRA_TIMESTAMP_COL_NAME] = modified.index
    return modified

# Prepares a dataset for analysis and prediction
# -> Timestamp | Heatpump power use | Indoor temperature | Outdoor temperature
# RETURNS: ( truncated_data_set, full_data_set )
def get_prepared_dataset(hvac_data, outenv_data, indenv_data):
    # Performs merge on two datasets on Timestamp (TODO: check if we don't have data skips)
    proper_dataset = pd.merge(hvac_data, outenv_data, on='Timestamp', how='inner')
    proper_dataset = pd.merge(proper_dataset, indenv_data, on='Timestamp', how='inner')
    print("Merged")
    print("Sorted")

    subset = proper_dataset[[POWER_COL_NAME, "OutEnv_OutdoorAmbTemp", INDOOR_TEMP_COL_NAME]]
    subset = subset.rename(columns={'OutEnv_OutdoorAmbTemp': OUTDOOR_TEMP_COL_NAME})
    subset = clean_data(subset)

    return (subset, proper_dataset)

# TODO:
# This assumes that all temperatures fall between -30 and 30
def clean_data(subset):
    subset = subset[subset[OUTDOOR_TEMP_COL_NAME].between(-30.0, 30.0)]
    subset = subset[subset[INDOOR_TEMP_COL_NAME].between(-30.0, 30.0)]
    return subset

def save_prepared_dataset(hvac_data, outenv_data, indenv_data, output_dataset_file):
    dataset = get_prepared_dataset(hvac_data, outenv_data, indenv_data)[0]
    dataset.to_csv(output_dataset_file, header=True)
    print("Saved to csv")