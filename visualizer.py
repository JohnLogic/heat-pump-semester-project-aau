import numpy as np
import matplotlib.pyplot as plt

import dataPrep as dpr

def visualize_data(dataset, temperatures):

    outdoorTemp = dataset[dpr.OUTDOOR_TEMP_COL_NAME]
    indoorTemp = dataset[dpr.INDOOR_TEMP_COL_NAME]
    avgPower = dataset[dpr.POWER_COL_NAME]
    times = dataset.index

    plt.close()
    plt.figure(1)

    plt.subplot(2, 1, 1)
    plt.plot(times, outdoorTemp, color="blue")
    plt.plot(times, indoorTemp, color="red")
    plt.title('Indoor and outdoor temperatures')

    plt.subplot(2, 1, 2)
    plt.plot(times, avgPower, color="red")
    plt.title('HP Power consumption')

    plt.grid()
    plt.show()
    plt.close()

    plt.figure(2)
    plt.title('Indoor temperatures')
    # TODO FIX: This is a dirty hack to make it actually show and not drop the Date error
    index_vals = np.arange(1, len(temperatures) + 1)
    temperatures = temperatures.assign(**{'Index': index_vals})
    temperatures.set_index('Index')

    for col in temperatures.columns:
        if ("Timestamp" in col or "TS" in col or "Index" in col):
            continue
        # HACK of eliminating the 'extreme values'
        temperatures = temperatures[temperatures[col].between(15.0, 30.0)]

    tempTimes = temperatures.index

    for col in temperatures.columns:
        if ("Timestamp" in col or "TS" in col or "Index" in col):
            continue
        plt.plot(tempTimes, temperatures[col], label = col)
    # plt.legend(loc='best')
    plt.grid()
    plt.show()
    plt.close()