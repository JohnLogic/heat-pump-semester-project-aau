# Instructions for running testRunner

See args
```bash
python testRunner.py --help
```

Prepare datasets (both an co-exist):
```bash
python testRunner.py --prepare-dataset hour   # prepare hour based dataset
python testRunner.py --prepare-dataset minute # prepare minute based dataset
```

Run linear regression:
```bash
python testRunner.py --linear-regression hour   # run on hour based dataset
python testRunner.py --linear-regression minute # run on minute based dataset
```


# Instructions for setting up

Need to install the following:
* Anaconda3
* Create an anaconda virtual environment (PyCharm does this)
* Install libraries:
```bash
conda install -p <path to virtual environment> numpy pandas scikit-learn matplotlib requests
```

## Setup initial datasets
* Nothing needed anymore. Datasets are automatically downloaded by testRunner
