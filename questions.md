# Relevant problems / questions

1. How to adequately define "Indoor temperature" (we have many datapoints)
    - as of 10-08, it's an average of all values for the given time
    - can be weighted avg and etc...
    - This can be a part of the problem statement, how we define the problem we want to actually solve
2. What do we do with *bad* data? 
    - There are points which are NaN or not legit