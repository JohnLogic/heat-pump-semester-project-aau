import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model, preprocessing
from sklearn.metrics import mean_squared_error, r2_score
import pandas as pd
from pylab import text
import model_base as base

# Todo 4 Figure out if several versions of mesuring the error yields significantly different result.

def linear_regression(timeperiod="default"):
    """
    :param timeperiod: The dataset one will be working from, 'all' is every file and if not found, default is selected.
    :return: void: prints the estimator pd.dataframe.
    """

    dataset_names = base.assign_datasets(timeperiod)
    estimators = pd.DataFrame()
    for key, value in dataset_names.items():

        # Load data
        filename = key
        df = base.loadfile(filename)
        print("Loading file: {}".format(filename))
        # Features
        X = df[['SumPower', 'OutdoorTemp', 'AvgIndoorTemp']].fillna(method='bfill')
        y = df[['AvgIndoorTemp']].fillna(method='bfill')

        regr = linear_model.LinearRegression()
        # Size of the rolling window, from t to t + rolling value
        if base.ROLLING_WINDOW_LR_ACTIVE:
            estimators = windowloop(X, estimators, filename, key, regr, y)
        else:
            estimators = linear_regression_helper(X, y, regr, 1, estimators, filename, key)

        # Make predictions using the testing set
        # pred = regr.predict(X_test2)
        plt.close('all')
    #estimators.to_csv('./images/TemporaryDump/Estimator_table_' + base.timestamp() + ".csv")
    print(estimators)


def linear_regression_helper(X, y, regr, windowsize, estimators, filename, key):
        """
        :param X: A pd.DataFrame of features.
        :param y: A pd.DataFrame of target.
        :param regr: The linear regression model.
        :param windowsize: How many time steps are considered for a given instance.
        :param estimators: A table of different error measures from all the previous experiments.
        :param filename: The file name, used as a partial string when giving the name of the plots.
        :param key: A dictionary key from base.DATA_ALL_GRANULARITIES for this given loop, currently key == filename.
        :return: Void. prints the configuration of the best model.
        """

        X_train, X_test, y_train, y_test = base.split_data(X, y, 0.8)

        """train the model, if type() not a np.ndarray then use .values.ravel() on the pd.DataFrame"""
        X_train_fit, y_train_fit = fit_type_safe(X_train, y_train)
        X_test_fit, y_test_fit = fit_type_safe(X_test, y_test)
        regr.fit(X_train_fit, y_train_fit)

        print("Predicting target values for window size: {}".format(windowsize))
        pred = regr.predict(X_test_fit)
        extension = "_reg_{}".format(windowsize)
        estimators = print_statistical(regr, y_test_fit, pred, estimators, "Lin"+extension+"_"+key)

        print("Plotting linear regression window size: {}".format(windowsize))
        plotting(y_test_fit, pred, filename + extension)
        return estimators


def windowloop(X, y, regr, estimators, filename, key, ):
    """
    This function is the loop for when we got 1 <= n iterations on the same granularity.
    :param X: A pd.DataFrame of features.
    :param y: A pd.DataFrame of target.
    :param regr: The linear regression model.
    :param estimators: A table of different error measures from all the previous experiments.
    :param filename: The file name, used as a partial string when giving the name of the plots.
    :param key: A dictionary key from base.DATA_ALL_GRANULARITIES for this given loop, currently key == filename.
    :return: A updated version of the estimator table with the newly added instances.
    """
    if base.ROLLING_WINDOW_LR_LOOP:
        for rolling in range(base.ROLLING_WINDOW_LR_LOOP_FLOOR, base.ROLLING_WINDOW_LR_SIZE):
            X_roll, y_roll = base.rolling_config(X, y, rolling)
            estimators = linear_regression_helper(X_roll, y_roll, regr, rolling, estimators, filename, key)
    else:
        rolling = base.ROLLING_WINDOW_LR_SIZE
        X_roll, y_roll = base.rolling_config(X, y, rolling)
        estimators = linear_regression_helper(X_roll, y_roll, regr, rolling, estimators, filename, key)
    return estimators


def fit_type_safe(X, y):
    """
    To read more about ravel source below:
    https://docs.scipy.org/doc/numpy/reference/generated/numpy.ravel.html
    :param X: A collection of features. Gets converted the to np.ndarray if it is any other type.
    :param y: A collection of targets. Gets converted the to np.ndarray if it is any other type.
    :return: X is a np.ndarray and y is an array of the same subtype as a, with shape (a.size,).
    """
    if not isinstance(X, np.ndarray):
        X = X.to_numpy()

    if not isinstance(y, np.ndarray):
        y = y.to_numpy().ravel()
    else:
        y = y.ravel()
    return X, y


def print_coeff(regr):
    """
    :param regr: The linear regression model.
    :return: Void, prints information about the model.
    """
    # Printing coefficients and intercept
    print('Coefficients: \n', regr.coef_)

    print('Intercept: \n', regr.intercept_)


def plot_lin(y_test, y_pred, filename, annotated, savefile):
    """
    :param y_test: The test target values.
    :param y_pred: The predicted target values.
    :param filename: The file name, used as a partial string when giving the name of the plots.
    :param annotated: If the plots will have some information attached to them. Or if just clean graphs.
    :param savefile: Whether the plot will be displayed or stored in a pre specified folder.
    :return: Void.
    """
    # Plot Config
    fig, ax = plt.subplots()
    ax.scatter(y_test, y_pred, edgecolors=(0, 0, 0))
    ax.plot([y_test.min(), y_test.max()], [y_test.min(), y_test.max()], 'k--', lw=4)

    # Inserting Text Labels
    ax.set_xlabel('Measured')
    ax.set_ylabel('Predicted')
    if annotated:
        text(0.8, 0.05, "Mean squared error: %.2f"
             % mean_squared_error(y_test, y_pred), ha='center', va='center', transform=ax.transAxes)
        text(0.8, 0.1, 'Variance score: %.2f' % r2_score(y_test, y_pred), ha='center', va='center',
             transform=ax.transAxes)

    if savefile:
        basepath = "images/TemporaryDump/"
        if "annotated" in filename:
            base.create_folders(basepath, ["/pdf/Annotated", "/png/Annotated"])
            pathpng= './images/TemporaryDump/png/Annotated/'
            pathpdf= './images/TemporaryDump/pdf/Annotated/'
        else:
            base.create_folders(basepath, ["/pdf/Clean", "/png/Clean"])
            pathpng= './images/TemporaryDump/png/Clean/'
            pathpdf= './images/TemporaryDump/pdf/Clean/'

        plt.savefig(pathpng + filename + base.timestamp() + ".png")
        plt.savefig(pathpdf + filename + base.timestamp() + ".pdf")

    else:
        # Plotting
        plt.show()


def plotting(y_test, pred, filename):
    """
    :param y_test: The test target values.
    :param pred: The predicted target values.
    :param filename: The file name, used as a partial string when giving the name of the plots.
    :return: Void.
    """
    # Plot output pipeline:
    plotname = "Lin_{}".format(filename)
    plot_lin(y_test, pred, plotname+"_annotated_", True, True)
    plot_lin(y_test, pred, plotname+"_clean_", False, True)


def print_statistical(y_test, pred, regr, estimators, indexname="NaN"):
    """
    :param y_test: The test target values.
    :param pred: The predicted target values.
    :param regr: The linear regression model.
    :param estimators: A table of different error measures from all the previous experiments.
    :param indexname: the name of the model
    :return: A updated version of the estimator table with the newly added instances.
    """
    # Printing coefficients and intercept
    print_coeff(regr)

    # Printing statistical
    estimators = estimators.append(base.generate_estimator_row(y_test, pred, indexname))
    #base.estimators_print(y_test, pred)
    return estimators


"""
The section below is commented out, since we currently do not use it. I have not deleted it, since it is not included
in any version control. And that it might be a viable option if the supervisor changes her mind.
It is the code that is relevant for predicting more than 1 step into the future, both approaches.
"""
####################Stashed Due To Only Only Predicting 1 Step Ahead####################
#
# def linear_regression(timeperiod="default"):
#
#     dataset_names = base.assign_datasets(timeperiod)
#     estimators = pd.DataFrame()
#     for key, value in dataset_names.items():
#         # Load data
#         filename = key
#         df = base.loadfile(filename)
#
#         #Only needed for day or hour data (raw format)
#         #df = base.preprocess_data(df)
#         #base.store_data_samples(df)
#         #df = base.resample_data(df, '15T')
#
#         # Features
#         X = df[['SumPower', 'OutdoorTemp', 'AvgIndoorTemp']]
#         y = df[['AvgIndoorTemp']]
#
#         print(X.head())
#         X_train, X_test, y_train, y_test = shift_data(X, y, 1)
#
#         # OPTIONAL:: Normalize independent feature values
#         # X = normalize_data_df(X)
#         for j in range(base.LR_SEP_SHIFT_FLOOR, base.LR_SEP_SHIFT_CEILING):
#             X_train, X_test, y_train, y_test = shift_data(X, y, j+1)
#
#             regr = linear_model.LinearRegression()
#
#             # Train the model using the training sets
#             regr.fit(X_train, y_train)
#
#             # Make predictions using the testing set
#             # pred = regr.predict(X_test2)
#             print("the value for j is {}".format(j))
#             # The execution of the complement model occurs meanwhile j == 1, not interested in composite behavior when j > 1
#             if j == 1:
#                 estimators = execute_comp_model(X_test, y_test, regr, estimators, filename, key)
#             estimators = execute_sep_model(X_test, y_test, regr, estimators, filename, key, j)
#
#         plt.close('all')
#
#     estimators.to_csv('./images/TemporaryDump/Estimator_table_' + base.timestamp() + ".csv")
#     print(estimators)
#
#
# def execute_comp_model(X, y, regr, estimators, filename, key):
#     if base.LR_COMP_ACTIVE:
#         for i in range(base.LR_COMP_STEP_FLOOR, base.LR_COMP_STEP_CEILING):
#             print("plotting composition {}".format(i))
#             pred = composite_hyperparam(X, 2*i, regr)
#             extension = "_comp_{}".format(2*i)
#             estimators = print_statistical(regr, y, pred, estimators, "Lin"+extension+"_"+key)
#             plotting(y, pred, filename + extension)
#     else:
#         pass
#     return estimators
#
#
# def execute_sep_model(X, y, regr, estimators, filename, key, incrementer):
#     if base.LR_SEP_ACTIVE:
#         print("plotting separation {}".format(incrementer))
#         pred = regr.predict(X)
#         extension = "_sep_{}".format(incrementer)
#         estimators = print_statistical(regr, y, pred, estimators, "Lin"+extension+"_"+key)
#         plotting(y, pred, filename+extension)
#     else:
#         pass
#     return estimators
#
# # Hacky Warning
# def composite_hyperparam(x_test, k, regr):
#     # Instantiate
#     predict_list = list()
#     for i in (range(x_test.shape[0])):
#         df = pd.DataFrame()
#         tempdf = x_test.iloc[[i]].copy()
#         pred = regr.predict(tempdf.values)
#
#         #Skip ahead with predicted value
#         for j in range(k):
#             df = df.append(insert_predict_into_df(tempdf, pred))
#             pred = regr.predict(df.tail(1).values)
#         predict_list.append(pred)
#
#     return np.array(predict_list).flatten()

#def insert_predict_into_df(x_test, pred):
#    x_test.iloc[0, x_test.columns.get_loc('AvgIndoorTemp')] = pred
#    return x_test
