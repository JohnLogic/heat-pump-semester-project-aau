
import requests, os

#
# Download data set files if they are not found in target_dir
#
def download(target_dir):
    for dataset in ("IndEnv", "OutEnv", "HVAC"):
        for type in ("hour", "minute"):
            url = "https://s3.amazonaws.com/nist-netzero/2014-data-files/%s-%s.csv" % (dataset, type)
            local_file = os.path.join(target_dir, "%s-%s.csv" % (dataset, type))
            if not os.path.exists(local_file):
                print("Downloading %s to %s, wait..." % (url, local_file))
                temp_file = local_file + ".tmp"
                with requests.get(url, stream=True) as r:
                    r.raise_for_status()
                    with open(temp_file, 'wb') as f:
                        for chunk in r.iter_content(chunk_size=8192):
                            if chunk:  # filter out keep-alive new chunks
                                f.write(chunk)
                os.rename(temp_file, local_file)
